# Sensito

Sensito is an application that copmutes the gamma from the readings of a sensito reader.

It has a graphical user interface (Windows only) and a command-line interface.

## Table of content

* [User documentation](#user-documentation)
  * [Prerequisites](#prerequisites)
  * [Install](#install)
  * [Graphical interface usage (Windows only)](#graphical-interface-usage-windows-only)
  * [Command-line usage](#command-line-usage)
* [Developer documentation](#developer-documentation)
  * [Prerequisites](#prerequisites-1)
  * [How to create a new release](#how-to-create-a-new-release)
* [How it works?](#how-it-works)

## User documentation

### Prerequisites

To run or compile this program, you will need [.NET 7](https://dotnet.microsoft.com/en-us/download/dotnet/7.0).

* On Windows: you need to install .NET Desktop Runtime 7
* On Linux: you need to install .NET Runtime 7 (as there is not graphical interface just yet)

### Install

To install Sensito:

1. Download the latest [release](https://gitlab.com/Creak/sensito/-/releases) for your platform
   (e.g. "Windows binaries (zip)")
2. Decompress it in a chosen location, such as `C:\sensito\`.
3. In this folder, execute `Sensito.Gui.exe` for the graphical interface (only available on
   Windows)

### Graphical interface usage (Windows only)

In order to use Sensito GUI (Graphical User Interface), you first need to have your sensito
readings in an Excel file, like this:

![Example of data in Excel](./docs/images/usage_excel.png)

The "B" column contains the sensito readings. See the multiple sheets at the bottom: you can
arrange your readings in multiple sheets if you want.

Now when you run `Sensito.Gui.exe`, and open your Excel file, you should see the same information:

![Example of data in Sensito GUI](./docs/images/usage_sensito_gui.png)

You can select the sheet (e.g. "5234") and the column where you sensito readings are (e.g. "B").

When you click on the button "Get gamma", it will compute the gamma based on those data and display
various other informations. The other informations are here to understand the context around the
computation.

* "Sheet" and "Column" are simply the sheet and column you selected to compute this gamma
* "Step value" is the density increase between values.
* "D-Min" and "D-Max" are the minimum and maximum densities found in the readings
* "Search range" is the range whithin the "flattest" part of the curve is found
* "Gamma range" is the actual range of the curve used to compute the gamma

### Command-line usage

```output
$ Sensito.Cli -h
Usage: Sensito.Cli [OPTIONS] FILE

Computes the gamma based on density values taken from a sensito output.

  FILE  Path to the file where the densito values are.
        Must be an Excel file (.xlsx).

  --step-value=VALUE         The step value.
  -s, --sheet-index=VALUE    The sheet index.
  -c, --column-index=VALUE   The column index.
  -h, --help                 Show this message and exit.
```

#### Examples

You can find an Excel file (.xlsx) in the [tests data directory](./tests/data/).

Here is an example of CLI usage:

```output
$ Sensito.Cli -s 3 sensito.xlsx
File: data/sensito.xlsx
  Gamma: 0.67

  Sheet: 5234
  Column: B
  Step value: 0.15
  D-Min: 0.24
  D-Max: 1.85
  Search range: 8 to 17
  Gamma range: 8 to 12
```

## Developer documentation

### Prerequisites

To run or compile this program, you will need [.NET 7](https://dotnet.microsoft.com/en-us/download/dotnet/7.0)
SDK.

### How to create a new release

Before creating a new release, define the release version. It will be referenced as `<VERSION>` in
this section.

To choose the new version please use [Semantic Versioning](https://semver.org/) (a.k.a. semver).

#### Prepare Linux version

1. In GitLab, create the tag `<VERSION>`
2. Wait for pipeline to finish
3. In the `create-linux-package` job in the pipeline, download the Linux package named
   `sensito-<VERSION>-linux.zip`

#### Prepare Windows version

1. Run `./build-scripts/build-win.sh <VERSION>`
2. See the built package in `build/sensito-<VERSION>-windows.zip`

#### Upload files for release

You'll need an [access token](https://gitlab.com/Creak/sensito/-/settings/access_tokens) with `api`
scope in order to [upload files](https://docs.gitlab.com/ee/api/projects.html#upload-a-file).

For both Linux and Windows package:

1. Send the file:

   ```sh
   curl --request POST \
       --header "PRIVATE-TOKEN: <your_access_token>" \
       --form "file=@path/to/sensito-<VERSION>-<windows_or_linux>.zip" \
       "https://gitlab.com/api/v4/projects/<your_project_id>/uploads
   ```

2. In the resulting JSON, copy the value of the property `full_path` and paste it in a text
   file, prefixed with `https://gitlab.com`

#### Create release

1. Create a [new release](https://gitlab.com/Creak/sensito/-/releases) based on the tag previously
   created
2. Use "\<VERSION\>" for the release title
3. Add changelog in the description
4. Create links, for each package:
   1. URL: Set the uploaded package URL noted previously
   2. Link title: "\<windows_or_linux\> binaries (zip)"

## How it works?

The algorithm is fairly simple, once it retrieved the curve values from the Excel file, it derives
the values twice. This gives the curve acceleration values. The closest to 0 the acceleration is,
the flattest it is. Not flat as in "horizontal", but flat as in "a straight line".

Then the algorithm determines the range in which it will search for the flattest part of the curve.
The lower bound of the range is defined by all the steps above 20% of the minimum density (D-Min).
The higher bound of the range is defined by all the steps below 20% of the maximum density (D-Max).
The percentages are defined against the difference between D-Max and D-Min.

Once the range defined, the algorithm takes the acceleration values and starts to search for the
flattest part of the curve. To do so, it searches for the 5 contiguous acceleration values with the
minimum accelerations.

So, to sum up, the algorithm defines a search range and then checks each range of 5 steps until it
finds the one with the lowest acceleration.
