namespace Sensito.Gui;

using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Win32;
using Sensito.Lib;
using Sensito.Lib.Models;

/// <summary>
/// Interaction logic for MainWindow.xaml
/// </summary>
public partial class MainWindow : Window
{
    private readonly double[] _stepValues = new[] { 0.15, 0.20 };

    public MainWindow()
    {
        InitializeComponent();

        // Add version to window title.
        var v = typeof(MainWindow).Assembly.GetName().Version ?? new Version("1.0.0.0");
        Title += $" {v.Major}.{v.Minor}.{v.Build}";

        // Set values in the StepValues combobox.
        cmbStepValues.ItemsSource = _stepValues.Select(x => x.ToString(CultureInfo.CurrentCulture));
        cmbStepValues.SelectedIndex = 0;
    }

    private DataSet? DataSet { get; set; }

    private List<IEnumerable<string>> SheetColumnsNames { get; } = new();

    private void RefreshColumnValuesLabel()
    {
        var labelStr = string.Empty;
        if (DataSet != null && cmbSheetNames.SelectedIndex != -1 && cmbColumnNames.SelectedIndex != -1)
        {
            var table = DataSet.Tables[cmbSheetNames.SelectedIndex];
            var columnValues = ExcelHelper.GetColumnValues(table, cmbColumnNames.SelectedIndex)
                .Where(x => !string.IsNullOrWhiteSpace(x))
                .ToArray();
            if (columnValues.Length > 4)
            {
                columnValues = columnValues[..3]
                    .Concat(new[] { "...", columnValues[^1] })
                    .ToArray();
            }
            labelStr = columnValues.Length switch
            {
                0 => $"No values",
                _ => $"Values: [{string.Join("; ", columnValues)}]",
            };
        }

        lblColumnValues.Content = labelStr;
    }

    private void btnOpenFile_Click(object sender, RoutedEventArgs e)
    {
        var openFileDialog = new OpenFileDialog()
        {
            Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*",
        };
        if (openFileDialog.ShowDialog() != true)
        {
            return;
        }

        // Clear UI
        cmbColumnNames.ItemsSource = null;
        cmbSheetNames.ItemsSource = null;

        // Clear in-memory data
        DataSet = null;
        SheetColumnsNames.Clear();

        // Fetch sheets names and their columns names
        var sheetNames = new List<string>();
        try
        {
            DataSet = SensitoReader.LoadFile(openFileDialog.FileName);

            for (var i = 0; i < DataSet.Tables.Count; ++i)
            {
                var table = DataSet.Tables[i];

                sheetNames.Add(table.TableName);

                var columnNames = new List<string>(table.Columns.Count);
                for (var j = 0; j < table.Columns.Count; ++j)
                {
                    // Get the column name (A, B, C, ...)
                    var columnName = ExcelHelper.GetColumnNameFromIndex(j);
                    columnNames.Add(columnName);
                }

                SheetColumnsNames.Add(columnNames);
            }
        }
        catch (Exception ex)
        {
            MessageBox.Show(
                "Can't open the file:" + Environment.NewLine + ex.Message + ".",
                "Sensito",
                MessageBoxButton.OK,
                MessageBoxImage.Exclamation);
        }

        // Update UI
        txtFilePath.Text = openFileDialog.FileName;
        cmbSheetNames.ItemsSource = sheetNames;
        cmbSheetNames.SelectedIndex = 0;
        txtOutput.Text = string.Empty;
    }

    private void cmbSheetNames_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
        if (cmbSheetNames.SelectedIndex == -1)
        {
            return;
        }

        var columnNames = SheetColumnsNames[cmbSheetNames.SelectedIndex];
        cmbColumnNames.ItemsSource = columnNames;

        var selectedColumnIdx = cmbColumnNames.SelectedIndex;
        if (!columnNames.Any())
        {
            // No column, no selection.
            selectedColumnIdx = -1;
        }
        else if (selectedColumnIdx == -1)
        {
            // If no column selected yet, take the second one (the first being the reading steps
            // most of the time).
            selectedColumnIdx = 1;
        }

        selectedColumnIdx = Math.Min(selectedColumnIdx, columnNames.Count());

        if (selectedColumnIdx != cmbColumnNames.SelectedIndex)
        {
            cmbColumnNames.SelectedIndex = selectedColumnIdx;
        }
        else
        {
            // Sheet changed, but column selection index is the same, force column values refresh.
            RefreshColumnValuesLabel();
        }
    }

    private void cmbColumnNames_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
        RefreshColumnValuesLabel();
    }

    private void btnGetGamma_Click(object sender, RoutedEventArgs e)
    {
        if (DataSet == null ||
            cmbSheetNames.SelectedIndex == -1 ||
            cmbColumnNames.SelectedIndex == -1 ||
            cmbStepValues.SelectedIndex == -1)
        {
            return;
        }

        SensitoDataSetReading result;
        try
        {
            var stepValue = _stepValues[cmbStepValues.SelectedIndex];
            var sensitoReader = new SensitoReader() { StepValue = stepValue };

            var sheetIdx = cmbSheetNames.SelectedIndex;
            var columnIdx = cmbColumnNames.SelectedIndex;
            result = sensitoReader.GetGammaFromDataSet(DataSet, sheetIdx, columnIdx);
        }
        catch (Exception ex)
        {
            MessageBox.Show(
                "Can't parse the file:" + Environment.NewLine + ex.Message + ".",
                "Sensito",
                MessageBoxButton.OK,
                MessageBoxImage.Exclamation);
            return;
        }

        var strBuilder = new StringBuilder(100);
        strBuilder.AppendLine(CultureInfo.InvariantCulture, $"Gamma: {result.Gamma:F2}");
        strBuilder.AppendLine();
        strBuilder.AppendLine(CultureInfo.InvariantCulture, $"Sheet: {result.SheetName}");
        strBuilder.AppendLine(CultureInfo.InvariantCulture, $"Column: {result.ColumnName}");
        strBuilder.AppendLine(CultureInfo.InvariantCulture, $"Step value: {result.StepValue:F2}");
        strBuilder.AppendLine(CultureInfo.InvariantCulture, $"D-Min: {result.DMin:F2}");
        strBuilder.AppendLine(CultureInfo.InvariantCulture, $"D-Max: {result.DMax:F2}");
        strBuilder.AppendLine(CultureInfo.InvariantCulture, $"Search range: {result.SearchRange.Start} to {result.SearchRange.End}");
        strBuilder.AppendLine(CultureInfo.InvariantCulture, $"Gamma range: {result.GammaRange.Start} to {result.GammaRange.End}");

        txtOutput.Text = strBuilder.ToString();
    }

    private void btnCopyClipboard_Click(object sender, RoutedEventArgs e)
    {
        Clipboard.SetDataObject(txtOutput.Text);
    }
}
