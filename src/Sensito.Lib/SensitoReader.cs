namespace Sensito.Lib;

using System.Data;
using ExcelDataReader;
using Sensito.Lib.Models;

public class SensitoReader
{
    private const double LowPercentage = 0.20;

    private const double HighPercentage = 0.20;

    private const int NumStepsInRange = 4;

    public SensitoReader()
    {
        System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
    }

    public double StepValue { get; set; } = 0.15;

    public static DataSet LoadFile(string filePath)
    {
        using var stream = File.Open(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
        using var reader = ExcelReaderFactory.CreateReader(stream);
        var dataSet = reader.AsDataSet();

        return dataSet;
    }

    public SensitoDataSetReading GetGammaFromFile(string filePath, int sheetIdx, int columnIdx)
    {
        var dataSet = LoadFile(filePath);
        return GetGammaFromDataSet(dataSet, sheetIdx, columnIdx);
    }

    public SensitoDataSetReading GetGammaFromDataSet(DataSet dataSet, int sheetIdx, int columnIdx)
    {
        if (sheetIdx >= dataSet.Tables.Count)
        {
            throw new InvalidDataException($"The sheet {sheetIdx} is out of range");
        }

        var table = dataSet.Tables[sheetIdx];
        if (columnIdx >= table.Columns.Count)
        {
            throw new InvalidDataException($"The column {columnIdx} is out of range");
        }

        List<double> values = new();
        for (var i = 0; i < table.Rows.Count; ++i)
        {
            var objValue = table.Rows[i][columnIdx];
            if (double.TryParse(objValue.ToString(), out var value))
            {
                values.Add(value);
            }
        }

        if (!values.Any())
        {
            throw new InvalidDataException("Couldn't find the values in the Excel file");
        }

        var reading = GetGammaFromValues(values);

        return new(reading)
        {
            SheetIndex = sheetIdx,
            SheetName = table.TableName,
            ColumnIndex = columnIdx,
            ColumnName = ExcelHelper.GetColumnNameFromIndex(columnIdx),
        };
    }

    public SensitoReading GetGammaFromValues(List<double> values)
    {
        if (!values.Any())
        {
            throw new InvalidDataException("Need at least one value");
        }

        var searchRange = GetSearchRange(values);
        var gammaRange = GetGammaRange(values, searchRange, 0.03);

        var gamma =
            (values[gammaRange.End] - values[gammaRange.Start]) /
            ((gammaRange.End.Value - gammaRange.Start.Value) * StepValue);

        var dMin = values.Min();
        var dMax = values.Max();

        return new()
        {
            Gamma = gamma,
            StepValue = StepValue,
            DMin = dMin,
            DMax = dMax,
            SearchRange = new Range(searchRange.Start.Value + 1, searchRange.End.Value + 1),
            GammaRange = new Range(gammaRange.Start.Value + 1, gammaRange.End.Value + 1),
        };
    }

    private static Range GetSearchRange(List<double> values)
    {
        var dMin = values.Min();
        var dMax = values.Max();

        var minThreshold = dMin + LowPercentage * (dMax - dMin);
        var startIndex = Math.Max(1, values.FindIndex(x => x > minThreshold));

        var maxThreshold = dMax - HighPercentage * (dMax - dMin);
        var endIndex = Math.Min(values.Count - 1, values.FindIndex(x => x > maxThreshold) - 1);

        return new Range(startIndex, endIndex);
    }

    private static List<double> GetDerivatives(IReadOnlyList<double> values)
    {
        List<double> derives = new();
        derives.Add(0.0);
        for (var i = 1; i < values.Count - 1; ++i)
        {
            var derivate = values[i + 1] - values[i - 1];
            derives.Add(derivate);
        }

        derives.Add(derives[derives.Count - 1]);

        return derives;
    }

    private static Range GetGammaRange(
        IReadOnlyList<double> values,
        Range searchRange,
        double accelerationThreshold)
    {
        var speeds = GetDerivatives(values);
        var accelerations = GetDerivatives(speeds);

        var start = searchRange.Start.Value;
        var bestStart = start;
        var lastAcc = double.MaxValue;
        while (start <= searchRange.End.Value - NumStepsInRange)
        {
            var acc = accelerations
                .Skip(start)
                .Take(NumStepsInRange)
                .Sum(x => Math.Abs(x));

            if (lastAcc > acc)
            {
                lastAcc = acc;
                bestStart = start;
            }

            ++start;
        }

        return new Range(bestStart, bestStart + NumStepsInRange);
    }
}
