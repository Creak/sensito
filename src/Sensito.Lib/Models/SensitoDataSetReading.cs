namespace Sensito.Lib.Models;

public record SensitoDataSetReading : SensitoReading
{
    public SensitoDataSetReading()
    {
    }

    public SensitoDataSetReading(SensitoReading reading)
    {
        Gamma = reading.Gamma;
        StepValue = reading.StepValue;
        DMin = reading.DMin;
        DMax = reading.DMax;
        SearchRange = reading.SearchRange;
        GammaRange = reading.GammaRange;
    }

    public int SheetIndex { get; init; }

    public string? SheetName { get; init; }

    public int ColumnIndex { get; init; }

    public string? ColumnName { get; init; }
}
