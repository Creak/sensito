namespace Sensito.Lib.Models;

public record SensitoReading
{
    public double Gamma { get; init; }

    public double StepValue { get; init; }

    public double DMin { get; init; }

    public double DMax { get; init; }

    public Range SearchRange { get; init; }

    public Range GammaRange { get; init; }
}
