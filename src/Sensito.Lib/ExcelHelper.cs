namespace Sensito.Lib;

using System.Data;

public static class ExcelHelper
{
    public static string GetColumnNameFromIndex(int idx)
    {
        var name = string.Empty;
        do
        {
            var chr = (char) ((int) 'A' + (idx % 26));
            name = chr + name;

            idx /= 26;
        }
        while (idx >= 26);

        if (idx >= 1)
        {
            // The most left digit must be decremented.
            // (so that, for instance, 26 gives "AA" and not "BA").
            var chr = (char) ((int) 'A' + (idx - 1));
            name = chr + name;
        }

        return name;
    }

    public static IEnumerable<string> GetColumnValues(DataTable table, int columnIdx)
    {
        List<string> values = new();
        for (var i = 0; i < table.Rows.Count; ++i)
        {
            var strValue = table.Rows[i][columnIdx].ToString();
            if (strValue != null)
            {
                if (double.TryParse(strValue, out var dblValue))
                {
                    values.Add($"{dblValue:F2}");
                }
                else
                {
                    values.Add(strValue);
                }
            }
        }

        return values;
    }
}
