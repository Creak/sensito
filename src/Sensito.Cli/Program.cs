namespace Sensito.Cli;

using System.Globalization;
using System.Text;
using Mono.Options;
using Sensito.Lib;

internal class Program
{
    private static int Main(string[] args)
    {
        var stepValue = 0.15;
        var sheetIdx = 0;
        var columnIdx = 1;
        var shouldShowHelp = false;

        // Command line options.
        var options = new OptionSet
        {
            { "step-value=", "The step value.", (int s) => stepValue = s },
            { "s|sheet-index=", "The sheet index.", (int s) => sheetIdx = s },
            { "c|column-index=", "The column index.", (int c) => columnIdx = c },
            { "h|help", "Show this message and exit.", h => shouldShowHelp = h != null },
        };

        List<string> extra;
        try
        {
            // Parse the command line
            extra = options.Parse(args);
        }
        catch (OptionException e)
        {
            ShowError(e.Message);
            Console.WriteLine();
            ShowUsage(options);
            return 1;
        }

        if (shouldShowHelp)
        {
            ShowUsage(options);
            return 0;
        }

        if (extra.Count == 0)
        {
            ShowError("Missing an argument, program takes at least one file.");
            Console.WriteLine();
            ShowUsage(options);
            return 1;
        }

        try
        {
            var filePath = extra.First();
            var sensitoReader = new SensitoReader()
            {
                StepValue = stepValue,
            };
            var result = sensitoReader.GetGammaFromFile(filePath, sheetIdx, columnIdx);

            var strBuilder = new StringBuilder(100);
            strBuilder.AppendLine(CultureInfo.InvariantCulture, $"File: {filePath}");
            strBuilder.AppendLine(CultureInfo.InvariantCulture, $"  Gamma: {result.Gamma:F2}");
            strBuilder.AppendLine();
            strBuilder.AppendLine(CultureInfo.InvariantCulture, $"  Sheet: {result.SheetName}");
            strBuilder.AppendLine(CultureInfo.InvariantCulture, $"  Column: {result.ColumnName}");
            strBuilder.AppendLine(CultureInfo.InvariantCulture, $"  Step value: {result.StepValue:F2}");
            strBuilder.AppendLine(CultureInfo.InvariantCulture, $"  D-Min: {result.DMin:F2}");
            strBuilder.AppendLine(CultureInfo.InvariantCulture, $"  D-Max: {result.DMax:F2}");
            strBuilder.AppendLine(CultureInfo.InvariantCulture, $"  Search range: {result.SearchRange.Start} to {result.SearchRange.End}");
            strBuilder.AppendLine(CultureInfo.InvariantCulture, $"  Gamma range: {result.GammaRange.Start} to {result.GammaRange.End}");

            Console.Write(strBuilder.ToString());

            return 0;
        }
        catch (Exception ex)
        {
            ShowError(
                "Something went wrong." + Environment.NewLine +
                $"  Exception: {ex.Message}");

            return 1;
        }
    }

    private static void ShowUsage(OptionSet options)
    {
        Console.WriteLine($"Usage: {AppDomain.CurrentDomain.FriendlyName} [OPTIONS] FILE");
        Console.WriteLine();
        Console.WriteLine("Computes the gamma based on density values taken from a sensito output.");
        Console.WriteLine();
        Console.WriteLine("  FILE  Path to the file where the densito values are.");
        Console.WriteLine("        Must be an Excel file (.xlsx).");
        Console.WriteLine();
        options.WriteOptionDescriptions(Console.Out);
    }

    private static void ShowError(string message)
    {
        Console.ForegroundColor = ConsoleColor.Red;
        Console.Write("Error: ");

        Console.ResetColor();
        Console.WriteLine(message);
    }
}
