#!/bin/sh
## Build Sensito Windows package

set -e

# Configure
if [ -z "$1" ] || (echo "$1" | grep -v '^[0-9]\+\.[0-9]\+\.[0-9]\+$'); then
    >&2 echo "ERROR: Need an argument of format 'x.y.z'"
    exit 1
fi

APP_VERSION="$1"
BUILD_DIR="build"

CONFIGURATION="Release"
RUNTIME="win-x64"
OUTPUT_DIR="${BUILD_DIR}/${RUNTIME}"

PACKAGE_NAME="sensito-${APP_VERSION}-windows"

# Clean
rm -rf "${OUTPUT_DIR}" "${BUILD_DIR}/${PACKAGE_NAME:?}/"

# Build
mkdir -p "${BUILD_DIR}"

dotnet publish \
    --configuration="${CONFIGURATION}" \
    --no-self-contained \
    --runtime="${RUNTIME}" \
    --output="${OUTPUT_DIR}" \
    "src/Sensito.Cli/Sensito.Cli.csproj" \
    -p:Version="${APP_VERSION}"

dotnet publish \
    --configuration="${CONFIGURATION}" \
    --no-self-contained \
    --runtime="${RUNTIME}" \
    --output="${OUTPUT_DIR}" \
    "src/Sensito.Gui/Sensito.Gui.csproj" \
    -p:Version="${APP_VERSION}"

mkdir -p "${BUILD_DIR}/${PACKAGE_NAME}"
cp -r "${OUTPUT_DIR}/"* LICENSE "${BUILD_DIR}/${PACKAGE_NAME}/"

#DOCKER_IMAGE="sensito-package"
#docker build -f "docker/Dockerfile" -t "${DOCKER_IMAGE}" .
#docker run --rm -v "/$PWD/build/:/app" "${DOCKER_IMAGE}" -r "${PACKAGE_NAME}.zip" "${PACKAGE_NAME}/"
