#!/bin/sh
## Build Sensito Windows installer
##
## TODO: the generated installer is considered as a virus by
##       other users' Windows.

set -e

# Configure
APP_VERSION="0.0.0"
BUILD_DIR="build"

PACKAGE_NAME="sensito"
PACKAGE_FILE_NAME="${PACKAGE_NAME}-${APP_VERSION}-windows-setup.exe"

INNOSETUP_FILE_IN="innosetup.iss.in"
INNOSETUP_FILE_OUT="innosetup.iss"
INNOSETUP_OUTPUT="sensito-setup.exe"
OUTPUT_DIR="${BUILD_DIR}/Output"
DOCKER_IMAGE="amake/innosetup:latest"

# Clean
rm -rf "${BUILD_DIR}/${INNOSETUP_FILE_OUT:?}" "${BUILD_DIR}/LICENSE" "${BUILD_DIR}/${PACKAGE_FILE_NAME:?}" "${OUTPUT_DIR}"

# Build
mkdir -p "${BUILD_DIR}"

./build-scripts/build-win-gui.sh

sed "s/@APP_VERSION@/${APP_VERSION}/" "${INNOSETUP_FILE_IN}" > "${BUILD_DIR}/${INNOSETUP_FILE_OUT}"
cp "LICENSE" "${BUILD_DIR}/"

docker run --rm --entrypoint '' -v "/$PWD/build/:/work/build" "${DOCKER_IMAGE}" iscc "${BUILD_DIR}/${INNOSETUP_FILE_OUT}"

mv "${OUTPUT_DIR}/${INNOSETUP_OUTPUT}" "${BUILD_DIR}/${PACKAGE_FILE_NAME}"
