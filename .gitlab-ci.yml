# Inspired from the CI/CD template located at:
# https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/dotNET-Core.gitlab-ci.yml

### Specify the Docker image
image: mcr.microsoft.com/dotnet/sdk:7.0

workflow:
  rules:
  - if: '$CI_PIPELINE_SOURCE == "push"'

stages:
- lint
- build
- publish
- package

variables:
  PACKAGE_NAME: 'sensito'
  LINUX_BUILD_FILE_NAME: '${PACKAGE_NAME}-${CI_COMMIT_TAG}-linux.zip'

### Stage: lint

lint-sh:
  stage: lint
  rules:
  - if: $CI_COMMIT_TAG == null
  needs: []
  image:
    name: koalaman/shellcheck-alpine:latest
    entrypoint: ['']
  script:
  - shellcheck **/*.sh

### Stage: build

build-and-test:
  stage: build
  rules:
  - if: $CI_COMMIT_TAG == null
  needs: []
  script:
  - dotnet build --configuration=Release
  - dotnet test --configuration=Release

### Stage: publish

.publish-cli:
  stage: publish
  rules:
  - if: $CI_COMMIT_TAG
  needs: []
  variables:
    SRC_PROJECT: 'src/Sensito.Cli/Sensito.Cli.csproj'
    OUTPUT_DIR: 'build/${RUNTIME}'
  script:
  - dotnet publish
      --configuration=Release
      --no-self-contained
      --runtime=${RUNTIME}
      --output=${OUTPUT_DIR}
      ${SRC_PROJECT}
      -p:Version="${CI_COMMIT_TAG}"
  - cp LICENSE ${OUTPUT_DIR}
  artifacts:
    paths:
    - ${OUTPUT_DIR}/

publish-linux-cli:
  extends: .publish-cli
  variables:
    RUNTIME: 'linux-x64'

### Stage: package

.create-package:
  stage: package
  rules:
  - if: $CI_COMMIT_TAG
  image: alpine:latest
  variables:
    GIT_STRATEGY: none
  before_script:
  - apk update
  - apk add zip curl
  script:
  - mkdir -p "${PACKAGE_NAME}"
  - cp -r "build/${RUNTIME}/"* "${PACKAGE_NAME}/"
  - zip -r "${PACKAGE_FILE_NAME}" "${PACKAGE_NAME}/"
  artifacts:
    paths:
    - ${PACKAGE_FILE_NAME}

create-linux-package:
  extends: .create-package
  needs:
  - publish-linux-cli
  variables:
    RUNTIME: 'linux-x64'
    PACKAGE_FILE_NAME: '${LINUX_BUILD_FILE_NAME}'
