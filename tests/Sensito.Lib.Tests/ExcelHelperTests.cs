namespace Sensito.Lib.Tests;

using FluentAssertions;

public class ExcelHelperTests
{
    private Random _random = new Random();

    [TestCase(0, "A")]
    [TestCase(1, "B")]
    [TestCase(25, "Z")]
    [TestCase(26, "AA")]
    [TestCase(26 + 25, "AZ")]
    [TestCase(26 * 2, "BA")]
    [TestCase(26 * 2 + 25, "BZ")]
    [TestCase(26 * 3 + 1, "CB")]
    [TestCase(26 * 26, "AAA")]
    [TestCase(26 * 26 + 1, "AAB")]
    [TestCase(26 * 26 + 26, "ABA")]
    [TestCase(26 * 26 * 26, "AAAA")]
    public void GetColumnNameFromIndex_ReturnsExpectedValue(int idx, string expectedResult)
    {
        // Act
        var result = ExcelHelper.GetColumnNameFromIndex(idx);

        // Assert
        result.Should().Be(expectedResult);
    }

    [Test]
    public void GetColumnValues_ReturnsValues()
    {
        // Arrange
        var table = _random.GetDataTable();
        List<string> expectedResult = new();
        for (var i = 0; i < table.Rows.Count; ++i)
        {
            var value = table.Rows[i][0].ToString();
            if (value != null)
            {
                if (double.TryParse(value, out var dblValue))
                {
                    expectedResult.Add($"{dblValue:F2}");
                }
                else
                {
                    expectedResult.Add(value);
                }
            }
        }

        // Act
        var result = ExcelHelper.GetColumnValues(table, 0);

        // Assert
        result.Should().BeEquivalentTo(expectedResult);
    }
}
