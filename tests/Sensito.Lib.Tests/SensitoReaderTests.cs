namespace Sensito.Lib.Tests;

using FluentAssertions;
using Sensito.Lib.Models;

public class SensitoReaderTests
{
    private const int SheetIdx = 0;

    private const int ColumnIdx = 1;

    private const double GammaApproximation = 0.01;

    private SensitoReader _sensitoReader = null!;

    [SetUp]
    public void Setup()
    {
        _sensitoReader = new SensitoReader();
    }

    [TestCaseSource(nameof(GetGammaFromFile_SourceData))]
    public void GetGammaFromFile_ReturnsExpectedValue(SensitoDataSetReading expectedResult)
    {
        // Arrange
        var filePath = @"data/sensito.xlsx";
        var sheetIdx = expectedResult.SheetIndex;
        var columnIdx = expectedResult.ColumnIndex;

        // Act
        var result = _sensitoReader.GetGammaFromFile(filePath, sheetIdx, columnIdx);

        // Assert
        result.SheetIndex.Should().Be(expectedResult.SheetIndex);
        result.SheetName.Should().Be(expectedResult.SheetName);
        result.ColumnIndex.Should().Be(expectedResult.ColumnIndex);
        result.ColumnName.Should().Be(expectedResult.ColumnName);
        result.Gamma.Should().BeApproximately(expectedResult.Gamma, GammaApproximation);
        result.StepValue.Should().Be(expectedResult.StepValue);
        result.DMin.Should().BeApproximately(expectedResult.DMin, GammaApproximation);
        result.DMax.Should().BeApproximately(expectedResult.DMax, GammaApproximation);
        result.SearchRange.Should().Be(expectedResult.SearchRange);
        result.GammaRange.Should().Be(expectedResult.GammaRange);
    }

    static private object[] GetGammaFromFile_SourceData =
    {
        new SensitoDataSetReading
        {
            SheetIndex = 0,
            SheetName = @"2234",
            ColumnIndex = ColumnIdx,
            ColumnName = @"B",
            Gamma = 0.60,
            StepValue = 0.15,
            DMin = 0.23,
            DMax = 1.93,
            SearchRange = new(7, 17),
            GammaRange = new(12, 16),
        },
        new SensitoDataSetReading
        {
            SheetIndex = 1,
            SheetName = @"2237",
            ColumnIndex = ColumnIdx,
            ColumnName = @"B",
            Gamma = 1.83,
            StepValue = 0.15,
            DMin = 0.10,
            DMax = 2.51,
            SearchRange = new(12, 17),
            GammaRange = new(13, 17),
        },
        new SensitoDataSetReading
        {
            SheetIndex = 2,
            SheetName = @"2302",
            ColumnIndex = ColumnIdx,
            ColumnName = @"B",
            Gamma = 2.48,
            StepValue = 0.15,
            DMin = 0.03,
            DMax = 4.05,
            SearchRange = new(11, 17),
            GammaRange = new(13, 17),
        },
        new SensitoDataSetReading
        {
            SheetIndex = 3,
            SheetName = @"5234",
            ColumnIndex = ColumnIdx,
            ColumnName = @"B",
            Gamma = 0.67,
            StepValue = 0.15,
            DMin = 0.24,
            DMax = 1.85,
            SearchRange = new(8, 17),
            GammaRange = new(8, 12),
        },
    };

    [Test]
    public void GetGammaFromFile_WithNull_ThrowsException()
    {
        // Act - Assert
        Assert.Throws<ArgumentNullException>(() => _sensitoReader.GetGammaFromFile(null!, SheetIdx, ColumnIdx));
    }

    [Test]
    public void GetGammaFromFile_WithEmptyFilePath_ThrowsException()
    {
        // Act - Assert
        Assert.Throws<ArgumentException>(() => _sensitoReader.GetGammaFromFile(string.Empty, SheetIdx, ColumnIdx));
    }

    [Test]
    public void GetGammaFromFile_WhenFileDoesntExist_ThrowsException()
    {
        // Arrange
        var filePath = @"does_not_exists.xlsx";

        // Act - Assert
        Assert.Throws<FileNotFoundException>(() => _sensitoReader.GetGammaFromFile(filePath, SheetIdx, ColumnIdx));
    }

    [Test]
    public void GetGammaFromValues_ReturnsExpectedValue()
    {
        // Arrange
        _sensitoReader.StepValue = 0.20;

        // Curve type: flat bottom, straight slope, flat top.
        var values = new List<double>();
        values.AddRange(Enumerable.Repeat(0.0, 4));
        values.AddRange(Enumerable.Range(1, 9).Select(x => (double)x));
        values.AddRange(Enumerable.Repeat(10.0, 4));

        var expectedResult = new SensitoReading
        {
            Gamma = 5.0,
            StepValue = 0.20,
            DMin = 0.0,
            DMax = 10.0,
            GammaRange = new(7, 11),
        };

        // Act
        var result = _sensitoReader.GetGammaFromValues(values);

        // Assert
        result.Gamma.Should().BeApproximately(expectedResult.Gamma, GammaApproximation);
        result.StepValue.Should().Be(expectedResult.StepValue);
        result.DMin.Should().BeApproximately(expectedResult.DMin, GammaApproximation);
        result.DMax.Should().BeApproximately(expectedResult.DMax, GammaApproximation);
        result.GammaRange.Should().Be(expectedResult.GammaRange);
    }

    [Test]
    public void GetGammaFromValues_WithNull_ThrowsException()
    {
        // Act - Assert
        Assert.Throws<ArgumentNullException>(() => _sensitoReader.GetGammaFromValues(null!));
    }

    [Test]
    public void GetGammaFromValues_WithEmptyArray_ThrowsException()
    {
        // Arrange
        var emptyList = new List<double>();

        // Act - Assert
        Assert.Throws<InvalidDataException>(() => _sensitoReader.GetGammaFromValues(emptyList));
    }
}
