namespace Sensito.Lib.Tests;

internal static class StringRandomizerExtensions
{
    public static char NextChar(this Random random)
    {
        return (char)random.Next(32, 127);
    }

    public static string GetString(this Random random)
    {
        var length = random.Next(5, 20);
        return new string(Enumerable.Range(0, length)
            .Select(n => random.NextChar())
            .ToArray());
    }
}
