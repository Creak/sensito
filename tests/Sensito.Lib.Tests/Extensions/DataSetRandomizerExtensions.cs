namespace Sensito.Lib.Tests;

using System.Data;

internal static class DataSetRandomizerExtensions
{
    public static float[] GetValues(this Random random)
    {
        var length = random.Next(5, 20);
        var values = Enumerable.Range(0, length)
            .Select(n => random.NextSingle())
            .ToArray();

        return values;
    }

    public static DataTable GetDataTable(this Random random)
    {
        var table = new DataTable(random.GetString());
        var column = table.Columns.Add();
        var values = random.GetValues();
        foreach (var value in values)
        {
            table.Rows.Add(value);
        }

        return table;
    }

    public static DataSet GetDataSet(this Random random)
    {
        var dataSet = new DataSet();
        dataSet.Tables.Add(random.GetDataTable());

        return dataSet;
    }
}
